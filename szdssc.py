#-*- coding: utf-8 -*-

import requests
import re
import threading
import time

requestSession = requests.Session()

def httpGetText(url, encoding=None):
    r = requestSession.get(url)
    if encoding is not None:
        r.encoding = encoding

    return r.text

def httpGetJson(url, encoding=None):
    r = requestSession.get(url)
    if encoding is not None:
        r.encoding = encoding
    return r.json()

def scanVideoList(url):
    global videoPageListLock, videoPageList
    
    html = httpGetText(url, "GBK")
    reVideoList = re.compile(r'<h3><a href=\"(.*?)\" target=\"_blank\">(.*?)<\/a><\/h3>')
    list = reVideoList.findall(html)
    
    videoPageListLock.acquire()

    for videoPage in list:
        videoPageList.append(videoPage)
        print(videoPage)
    
    videoPageListLock.release()

def scanVideoListMain():
    scanVideoList('http://www.csztv.com/wldst/zdjm/szdssc/index.shtml')
    for i in range(2, 21):
        scanVideoList('http://www.csztv.com/wldst/zdjm/szdssc/index_%d.shtml' % (i))

def parseVideoPage(url):
    #http://p.bokecc.com/servlet/getvideofile?vid=19D5FAF41E7021809C33DC5901307461&siteid=C66E7E70C2CBCFD2&divid=cc_video_19D5FAF41E7021809C33DC5901307461_9782764&width=540&useragent=other&version=20140214&hlssupport=0&callback=cc_js_Player.showPlayerView&r=4295335.509814322
    #http://p.bokecc.com/servlet/playinfo?uid=C66E7E70C2CBCFD2&vid=19D5FAF41E7021809C33DC5901307461&lid=&pid=9B57F42F54E3185C&pt=1&pm=spark&ci=&isp=%2D1&m=1&pp=false&d=www%2Ecsztv%2Ecom&fv=WIN%2017%2C0%2C0%2C169&uu=F1F518D7844120F87E823E511842FD317EAE3A52&vc=null&version=20140214&rnd=1343
    html = httpGetText(url, "GBK")
    rePlayerInfo = re.compile(r"http:\/\/union.bokecc.com\/player\?vid=(.*?)&siteid=(.*?)&autoStart=(.*?)&width=(.*?)&height=(.*?)&playerid=(.*?)&playertype=1")
    vid, siteid, autoStart, width, height, playerid = rePlayerInfo.findall(html)[0]
    reTitle = re.compile(r"<title>(.*?)<\/title>")
    title = reTitle.findall(html)[0]
    print(vid)
    print(siteid)
    print(playerid)
    print(title)
    playInfoUrl = "http://p.bokecc.com/servlet/playinfo?uid=" + siteid + "&vid=" + vid + "&lid=&pid=" + playerid + "&pt=1&pm=spark&ci=&isp=%2D1&m=1&pp=false&d=www%2Ecsztv%2Ecom&fv=WIN%2017%2C0%2C0%2C169&uu=F1F518D7844120F87E823E511842FD317EAE3A52&vc=null&version=20140214&rnd=1343"
    playInfoHtml = httpGetText(playInfoUrl)
    rePlayUrl = re.compile(r"playurl=\"(.*?)\"")
    reUpid = re.compile(r"<UPID>(.*?)<\/UPID>")
    upid = reUpid.findall(playInfoHtml)[0]
    reVideoUrl = re.compile(r"videoURL=\"union_(.*?)\"")
    videoUrl = reVideoUrl.findall(playInfoHtml)[0]
    print(upid)
    httpGetText("http://play.bokecc.com/servlet/PlayVideo?videoID=" + vid + "&userID=" + siteid + "&serverDomain=http%3A%2F%2Fcp12%2Ec120%2Eplay%2Ebokecc%2Ecom%2F&videourl=" + videoUrl + "&data=1430362542149&random=6554241")
    index = 1;
    for url in rePlayUrl.findall(playInfoHtml):
    	r = requests.get(url + "&upid=" + upid + "&pt=0&pi=1", headers = { "referer": url})
    	with open("%s%d" % (title, index), 'wb') as fd:
    		for chunk in r.iter_content(1000):
        		fd.write(chunk)
    	index = index + 1

def parseVideoPageMain():
    global videoPageListLock, videoPageList, videoList, videoListLock
    time.sleep(5)
    while 1:
        time.sleep(1)
        videoPageUrl = None
        videoPageName = None
        videoPageListLock.acquire()
        if (len(videoPageList) > 0):
            videoPageUrl, videoPageName = videoPageList[0]
            videoPageList.remove(videoPageList[0])
        videoPageListLock.release()

        if videoPageUrl is None:
            break
        parseVideoPage("http://www.csztv.com" + videoPageUrl)

def downloadVideo(url):
    pass

def downloadVideoMain():
    pass

if __name__ == "__main__":
    global videoPageList, videoPageListLock, videoList, videoListLock
    videoPageListLock = threading.Lock()
    videoListLock = threading.Lock()
    videoPageList = []
    videoList = []

    threads = []

    threads.append(threading.Thread(target=scanVideoListMain))
    threads.append(threading.Thread(target=parseVideoPageMain))
    threads.append(threading.Thread(target=downloadVideoMain))

    for t in threads:
        t.start()

    for t in threads:
        t.join()
    

    

